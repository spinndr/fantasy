=== Sleeper App Cheats ===

### This tool provides recommendations on roster optimization 
The tool generates roster.json and rankings.html which displays optimized rosters

Example of rankings.html
![Demo of rankings.html](demo.PNG)


### Download rankings data
Player rankings are retrieved from a third party website Oleg Borischen

> node etl.js

### Optimize Sleepr App league rosters
Then each roster is sorted based on these rankings to recommend the optimal roster

> node ff.js

### Operation
Each week, update ff.js and set "currentWeek"
Each week, update nfl.json roster from Sleeper API

Each season, update **byes.json** with the bye week for each team.

