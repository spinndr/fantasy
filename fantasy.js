const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const fs = require("fs")

let nfl = require("./nfl.rankings.json")

let url = 'https://www.fantasynerds.com/nfl/waiver-wire/ppr'
// parse
findPlayers = (position) => {
     let rankings = Array.from(position.parentElement.children).filter(elem => elem.className.indexOf("pad") >= 0)
     let players = rankings.flatMap(rank => Array.from(rank.getElementsByTagName("a")))
     return players.map(player => {  
          return { name: player.text, href: player.href  } 
     })
}

main = async () => {
     // request
     let html = await fetch(url).then(response => response.text())
     // console.log(`html: `, html)

     let jsDOM = new JSDOM(html)
     // console.log(htmlDOM)

     let positions = Array.from(jsDOM.window.document.querySelectorAll("h4.passion.toptop"))
     positions.shift()

     let pickups = {}
     positions.forEach(position => {
          let players = findPlayers(position)
          let name = position.textContent.trim()
          pickups[name] = players
     })
     // console.log(pickups)

     // map pickups to nfl
     for(var key in pickups) {
          let players = pickups[key]
          players = players.map(player => {
               let foundPlayer = Object.values(nfl).find(hydratedPlayer => {
                    return hydratedPlayer.full_name === player.name
               })
               foundPlayer = foundPlayer ?? {}
               return {
                    ...player,
                    full_name: foundPlayer.full_name,
                    ...foundPlayer
               }
          })
          pickups[key] = players.map(player => `${player.full_name} (${player.position})`)
     }

     console.log('Pickups')
     console.log(pickups)
}

main()

