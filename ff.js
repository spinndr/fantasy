const fs = require("fs")

// optimize roster
console.log("Reading nfl.rankings.json...")
let nfl = require("./nfl.rankings.json")

const SEARCH = {
    leagues: [
        {
            name: "Chester Cheeto",
            url: "https://api.sleeper.app/v1/league/1124850806223290368",
            rosters: "/rosters",
            owners: [
                {
                    name: "Plorp",
                    owner_id: "620106552183013376",
                },
                {
                    name: "Abjacs",
                    owner_id: "609215010190716928",
                },
                {
                    name: "cthompson222",
                    owner_id: "1132738598312280064",
                }
            ],
            currentWeek: "16",
        },
        {
            name: "Degenerates Year 7",
            url: "https://api.sleeper.app/v1/league/1106073124904407040",
            rosters: "/rosters",
            owners: [
                {
                    name: "Plorp",
                    owner_id: "620106552183013376"
                },
                {
                    name: "Abjacs",
                    owner_id: "609215010190716928"
                }
            ],
            currentWeek: "16",
        }
    ]
}

optimizeRoster = (playerIds, rosterPositions, currentWeek) => {
    // default roster positions
    let roster_positions = rosterPositions ?? ["QB", "RB", "RB", "WR", "WR", "TE", "FLEX", "FLEX", "FLEX", "K", "DEF"]
    let players = playerIds?.map(playerId => nfl[playerId]) ?? []
    // active players
    players = players.filter(player => (player?.status === "Active" && player?.injury_status !== "Out") || (player?.active
        && player?.position === "DEF"))

    // DEBUG
    // console.log(players.map(p => p.full_name))
    let optimizedRoster = []

    for (const position of roster_positions) {
        // filter out players already in roster
        players = players.filter(player => player.active)
            .filter(player => !(player.bye_week === currentWeek))
            .filter(player => !optimizedRoster.some(r => r && r.player_id && r.player_id === player.player_id))

        // find players with position
        const positionPlayers = players.filter(player => player.fantasy_positions.includes(position))
            .map(player => player.fantasyWorld?.oleg[position] ?? player)
            .filter(x => x)
            .sort((a, b) => { return (a.tier_ranking ?? 9999) - (b.tier_ranking ?? 9999) })

        // console.log(`positionPlayers for ${position}`)
        // console.log(JSON.stringify(positionPlayers, null, 2))
        const match = positionPlayers.find(() => true)
        const foundPlayer = nfl[match?.player_id] ?? { full_name: "--", position: position }
        optimizedRoster = [...optimizedRoster, foundPlayer]
    }

    return optimizedRoster
}

optimizeRosterForSleeper = async (owner, rosterURL, currentWeek) => {
    const { owner_id, name } = owner

    // GET Rosters for league
    const rosters = await fetch(rosterURL).then(response => response.json())

    // Find roster for owner_id
    const roster = rosters.find(roster => roster.owner_id === owner_id)
    console.log(`Optimizing roster for ${name} (owner_id: ${owner_id})`)

    let roster_positions = ["QB", "RB", "RB", "WR", "WR", "TE", "FLEX", "FLEX", "FLEX", "K", "DEF"]
    return optimizeRoster(roster.players, roster_positions, currentWeek)
}

function makeHTML(leagues) {
    const generatedAt = (new Date().toLocaleString("en-US", { timeZone: "America/Chicago" }))
    const forWeek = (leagues.length > 0 ? leagues[0].currentWeek : "--")
    const makeOwnerHTML = (owner) => {
        return `<div style="margin-top: 1rem; margin-right: 1rem;"><h2>Roster for ${owner.name} owner_id: ${owner.owner_id}</h2>
            <table>
            <thead>
                <td>Position</td>
                <td>Name</td>
            </thead>
            <tbody>
            ${owner.roster.map(player => { return `<tr><td>${player.position}</td><td>${player.full_name}</td></tr>` }).join("")}
            </tbody>
            </table></div>`
    }

    return `
    <html>
    <head>
        <title>Fantasy Football</title>
        <style type="text/css">
        thead {
            font-weight: 600;
            border: 1px solid black;
            background-color: black;
            color: white;
            font-size: 1em;
        }

        tr {
            font-size: 2em;
        }

        body {
            padding: 2em;
            padding-bottom: 4em;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            padding: 0;
            margin: 0;
        }

        table {
            width: 100%;
            table-layout: fixed;
        }

        body {
            margin: auto;
            max-width: 50rem;
        }
    </style>
    </head>
    <body>
    <div style="display: flex; flex-direction: column;">
    <div style="flex: 1; flex-direction: row;">
        <h1>Week ${forWeek}</h1>
        <h1>Generated on ${generatedAt} CST</h1>
    </div>
    <div style="display: flex; flex: 1; flex-direction: column;">
    ${leagues.map(league => {
        return `
            <div style="display: flex; flex: 1; flex-direction: column; margin: 2rem 0;">
                <div style="margin-top: 1rem;"><h1>League: ${league.name}</h1></div>\n
                ${league.owners.map(owner => makeOwnerHTML(owner)).join("")
            }
            </div>`
    }).join("")
        }
    </div>
    </body>
</html>`
}

async function main() {
    for (var league of SEARCH.leagues) {
        console.log(`League ${league.name} Week: ${league.currentWeek}`)
        for (var owner of league.owners) {
            if (league.url) {
                const rosterUrl = league.url + league.rosters
                const newRoster = (await optimizeRosterForSleeper(owner, rosterUrl, league.currentWeek)).map(p => { return { position: p.position, full_name: p.full_name } })

                console.log(`Roster`)
                console.log(newRoster)

                owner.roster = newRoster
            }
            else {
                // custom
                const playerIds = owner.players
                const rosterPositions = owner.rosterPositions
                const newRoster = (await optimizeRoster(playerIds, rosterPositions, league.currentWeek)).map(p => { return { position: p.position, full_name: p.full_name } })

                console.log(`Roster`)
                console.log(newRoster)

                owner.roster = newRoster
            }
        }
    }

    // console.log(JSON.stringify(SEARCH.leagues, null, 2))
    fs.writeFileSync("roster.json", JSON.stringify(SEARCH.leagues, null, 2))

    const htmlPage = makeHTML(SEARCH.leagues)
    fs.writeFileSync("rankings.html", htmlPage)
}

main()
