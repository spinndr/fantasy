const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const fs = require("fs")
const http = require("https")

const awsBucketURL = "https://s3-us-west-1.amazonaws.com/fftiers/out/"
let rankings = {"QB": "text_QB.txt", "RB": "text_RB-PPR.txt", "WR": "text_WR-PPR.txt", "FLEX": "text_FLX-PPR.txt", "TE": "text_TE-PPR.txt", "DEF": 
"text_DST.txt"}

// https://stackoverflow.com/a/17676794
const download = function(url, dest, cb) {
     let file = fs.createWriteStream(dest);
     http.get(url, function(response) {
          response.pipe(file);
          file.on('finish', function() {
               file.close(cb);
          });
     });
}
   

async function main() {
     Object.keys(rankings).forEach(key => {
          const filename = rankings[key]
          rankings[key] = {
               filename: filename,
               url: `${awsBucketURL}${filename}`
          }
     })

     // download oleg boris chen files
     console.log(`Downloading Oleg Boris FF files`)
     for (var key in rankings) {
          const { filename, url } = rankings[key]
          console.log(`Downloading ${filename}`)
          await download(url, filename)
     }
     // wait 1 secs for files to close
     await new Promise(resolve => setTimeout(resolve, 1000));

     for (var key in rankings) {
          let rankedPlayers = []
          let { filename } = rankings[key];
          let rawString = fs.readFileSync(filename, "utf8")
          // console.log(`OPENing.... ${filename}`)
          // console.log(rawString)

          // parse file
          let tiers = rawString.split("\n").filter(t => t)
          tiers.forEach(tier => {
               let [tierNum, players] = tier.split(":")
               tierNum = tierNum.replace("Tier ", "")
               // console.log(tierNum)
               // console.log(players)

               let rankings = players.split(",").map((player, index) => {
                    return {
                         full_name: player.trim().replace(" III", "").replace(" Jr.", ""),
                         tier: Number(tierNum),
                         tier_position: index + 1,
                         tier_ranking: Number(`${Number(tierNum) + ((index + 1)/10)}`),
                         position: key,
                    }
               })
               rankedPlayers = [...rankedPlayers, ...rankings]
          })
          
          rankings[key] = {
               filename: filename,
               position: key,
               players: rankedPlayers,
          }
     }
     // console.log(`Ranked players: ${JSON.stringify(rankings, null, 2)}`)

     // GET ALL PLAYERS
     // https://api.sleeper.app/v1/players/nfl
     let nfl = require("./nfl.json")
     let teams = require("./byes.json")

     for (const [key, value] of Object.entries(nfl)) {
          // add FLEX to fantasy_positions
          let fantasyPositions = value.fantasy_positions
          if(["TE", "RB", "WR"].includes(value.position)) {
               fantasyPositions = [ ...value.fantasy_positions, "FLEX" ]
          }

          // add full_name for DST
          nfl[key] = { ...value, fantasy_positions: fantasyPositions, full_name : `${value.first_name} ${value.last_name}`}

          // add bye_week to DST
          const team = teams.find(t => t.team === value.team)
          if (team) {
               nfl[key] = { ...nfl[key], bye_week: team.bye_week }
          }

          // add rankings data
          let matchedPlayers = Object.values(rankings).map(ranking => ranking.players).flatMap(arr => arr).filter(player => player.full_name === value.full_name)
          if (matchedPlayers.length > 0) {
               let ranking = { fantasyWorld: { oleg: { }, }}
               for(var player of matchedPlayers) {
                    ranking.fantasyWorld.oleg[player.position] = { ...player, player_id: key }
               }
               // console.log(`Ranking`, JSON.stringify(ranking, null, 2))
               nfl[key] = { ...ranking, ...nfl[key] }
          }
     }
     // console.log({ nfl })
     console.log(`Wrote output to ${"nfl.rankings.json"}`)
     fs.writeFileSync("nfl.rankings.json", JSON.stringify(nfl, null, 2))
}

main()